-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-10-2020 a las 16:21:19
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bicicleteria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bicicletas`
--

CREATE TABLE `bicicletas` (
  `id` int(11) NOT NULL,
  `codigo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `bicicletas`
--

INSERT INTO `bicicletas` (`id`, `codigo`) VALUES
(9, 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `rol` varchar(30) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `password` varchar(12) NOT NULL,
  `usuario` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`rol`, `nombre`, `password`, `usuario`) VALUES
('bicicletero', 'Bicicletero 1', '1234', 'bicicletero1'),
('encargado', 'Encargado 1', '1234', 'encargado1'),
('vendedor', 'Vendedor 1', '1234', 'vendedor1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `piezas`
--

CREATE TABLE `piezas` (
  `id` int(11) NOT NULL,
  `pieza` varchar(30) NOT NULL,
  `codigo` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `piezas`
--

INSERT INTO `piezas` (`id`, `pieza`, `codigo`) VALUES
(2, 'Asiento', '2'),
(46, 'Asiento', 'F'),
(68, 'Cuadro', 'A'),
(69, 'Cuadro', 'A'),
(70, 'Cuadro', 'A'),
(71, 'Cuadro', 'A'),
(72, 'Cuadro', 'A'),
(73, 'Cuadro', 'A'),
(82, 'Rueda', 'A'),
(83, 'Rueda', 'A'),
(84, 'Rueda', 'A'),
(85, 'Rueda', 'A'),
(86, 'Rueda', 'A'),
(87, 'Rueda', 'A'),
(88, 'Rueda', 'A'),
(89, 'Rueda', 'A'),
(90, 'Rueda', 'A'),
(91, 'Rueda', 'A'),
(92, 'Rueda', 'A'),
(93, 'Rueda', 'A'),
(102, 'Pedal', 'A'),
(103, 'Pedal', 'A'),
(104, 'Pedal', 'A'),
(105, 'Pedal', 'A'),
(106, 'Pedal', 'A'),
(107, 'Pedal', 'A'),
(108, 'Pedal', 'A'),
(109, 'Pedal', 'A'),
(110, 'Pedal', 'A'),
(111, 'Pedal', 'A'),
(112, 'Pedal', 'A'),
(113, 'Pedal', 'A'),
(118, 'KitMecanico', 'A'),
(119, 'KitMecanico', 'A'),
(120, 'KitMecanico', 'A'),
(121, 'KitMecanico', 'A'),
(122, 'KitMecanico', 'A'),
(123, 'KitMecanico', 'A'),
(124, 'Rueda', 'A'),
(125, 'Rueda', 'A'),
(126, 'Rueda', 'A'),
(127, 'Rueda', 'A'),
(128, 'Rueda', 'A'),
(129, 'Rueda', 'A'),
(130, 'Rueda', 'A'),
(131, 'Rueda', 'A'),
(132, 'Rueda', 'A'),
(133, 'Rueda', 'A'),
(134, 'Rueda', 'A'),
(135, 'Rueda', 'A'),
(136, 'Rueda', 'A'),
(137, 'Rueda', 'A'),
(138, 'Rueda', 'A'),
(139, 'Rueda', 'A'),
(140, 'Rueda', 'A'),
(141, 'Rueda', 'A'),
(142, 'Rueda', 'A'),
(143, 'Rueda', 'A'),
(148, 'Asiento', 'A'),
(149, 'Asiento', 'A'),
(150, 'Asiento', 'A'),
(151, 'Asiento', 'A'),
(152, 'Asiento', 'A'),
(153, 'Asiento', 'A'),
(154, 'Asiento', 'A'),
(155, 'Asiento', 'A'),
(156, 'Asiento', 'A'),
(157, 'Asiento', 'A'),
(158, 'Asiento', 'A'),
(159, 'Asiento', 'A'),
(160, 'Asiento', 'A'),
(161, 'Asiento', 'A'),
(162, 'Asiento', 'A'),
(163, 'Asiento', 'A'),
(164, 'Asiento', 'A'),
(165, 'Asiento', 'A'),
(166, 'Asiento', 'A'),
(167, 'Asiento', 'A'),
(172, 'Manubrio', 'A'),
(173, 'Manubrio', 'A'),
(174, 'Manubrio', 'A'),
(175, 'Manubrio', 'A'),
(176, 'Manubrio', 'A'),
(177, 'Manubrio', 'A'),
(178, 'Manubrio', 'A'),
(179, 'Manubrio', 'A'),
(180, 'Manubrio', 'A'),
(181, 'Manubrio', 'A'),
(182, 'Manubrio', 'A'),
(183, 'Manubrio', 'A'),
(184, 'Manubrio', 'A'),
(185, 'Manubrio', 'A'),
(186, 'Manubrio', 'A'),
(187, 'Manubrio', 'A'),
(188, 'Manubrio', 'A'),
(189, 'Manubrio', 'A'),
(190, 'Manubrio', 'A'),
(191, 'Manubrio', 'A'),
(192, 'Manubrio', 'A'),
(193, 'Manubrio', 'A'),
(194, 'Manubrio', 'A'),
(195, 'Manubrio', 'A'),
(196, 'Manubrio', 'A'),
(197, 'Manubrio', 'A'),
(198, 'Manubrio', 'A'),
(199, 'Manubrio', 'A'),
(200, 'Manubrio', 'A'),
(201, 'Manubrio', 'A'),
(202, 'Manubrio', 'A'),
(203, 'Manubrio', 'A'),
(204, 'Asiento', 'y'),
(205, 'Asiento', 'y'),
(206, 'Asiento', 'y'),
(207, 'Asiento', 'y'),
(208, 'Asiento', 'y');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id` int(11) NOT NULL,
  `codigo` varchar(50) DEFAULT NULL,
  `fecha` date DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id`, `codigo`, `fecha`) VALUES
(2, 'A', '2020-10-05'),
(3, 'F', '2020-10-05'),
(4, 'B', '2020-10-05'),
(5, 'A', '2020-10-05'),
(6, 'A', '2020-10-05'),
(7, 'A', '2020-10-10');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bicicletas`
--
ALTER TABLE `bicicletas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD UNIQUE KEY `usuario` (`usuario`);

--
-- Indices de la tabla `piezas`
--
ALTER TABLE `piezas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bicicletas`
--
ALTER TABLE `bicicletas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `piezas`
--
ALTER TABLE `piezas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=209;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
