<%-- 
    Document   : registro
    Created on : 15 sep. 2020, 21:33:27
    Author     : Liz
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrar usuario</title>
    </head>
    <body>
        <nav>
            <a href="ControladorEncargado?action=home">Inicio</a>
            <a href="ControladorEncargado?action=altapieza">Alta de pieza</a>
            <a href="ControladorEncargado?action=listadopiezas">Listado de piezas</a>
            <a href="ControladorEncargado?action=listadoventas">Listado de bicicletas vendidas</a>
            <a href="ControladorEncargado?action=registrouser">Registrar usuario</a>
            <a href="ControladorEncargado?action=usuariosreg">Usuarios registrados</a>
            <a href="ControladorLogout">Salir</a>

        </nav>

        <h1>Bicicleteria</h1>
        
        <h2>Registrar usuario</h2>
        
        <form method="post" action="ControladorEncargado?action=guardaruser">
            <label for="nombre">Nombre</label>
            <input type="text" id="nombre" name="nombre">
            
            <label for="usuario">Usuario</label>
            <input type="text" id="usuario" name="usuario">

            <label for="password">Contraseña</label>
            <input type="password" id="password" name="password">
            
            <label for="rol">Rol</label>
            <select id="rol" name="rol">
                <option value="bicicletero">Bicicletero</option>
                <option value="vendedor">Vendedor</option>
            </select>
            
            <input type="submit" value="Registrar">
        </form>
        
    </body>
</html>
