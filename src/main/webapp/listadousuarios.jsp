<%-- 
    Document   : listadousuarios
    Created on : 21 sep. 2020, 19:27:39
    Author     : Liz
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listado de usuarios</title>
    </head>
    <body>
    <nav>
        <a href="ControladorEncargado?action=home">Inicio</a>
        <a href="ControladorEncargado?action=altapieza">Alta de pieza</a>
        <a href="ControladorEncargado?action=listadopiezas">Listado de piezas</a>
        <a href="ControladorEncargado?action=listadoventas">Listado de bicicletas vendidas</a>
        <a href="ControladorEncargado?action=registrouser">Registrar usuario</a>
        <a href="ControladorEncargado?action=usuariosreg">Usuarios registrados</a>
        <a href="ControladorLogout">Salir</a>
    </nav>
        
    <h1>Bicicleteria</h1>
      
    <h2>Usuarios registrados</h2>
    <table>
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Rol</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="usuario" items="${usuarios}">
                <tr>
                    <td>${usuario.nombre}</td>
                    <td>${usuario.rol}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    </body>
</html>
