<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Bicicleteria</title>
  </head>
  <body class="home-encargado">
    <nav>
        <a href="ControladorEncargado?action=home">Inicio</a>
        <a href="ControladorEncargado?action=altapieza">Alta de pieza</a>
        <a href="ControladorEncargado?action=listadopiezas">Listado de piezas</a>
        <a href="ControladorEncargado?action=listadoventas">Listado de bicicletas vendidas</a>
        <a href="ControladorEncargado?action=registrouser">Registrar usuario</a>
        <a href="ControladorEncargado?action=usuariosreg">Usuarios registrados</a>
    </nav>
    <div class="div-encargado"> 
    <h1>Encargado</h1>
    <p>
        Hola ${nombre}!
    </p>

    <form method="post" action="ControladorLogout">
      <input type="submit" value="Salir" >
    </form>
    </div> 
  </body>
</html>
