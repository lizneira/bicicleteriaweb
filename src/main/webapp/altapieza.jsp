<%-- 
    Document   : altapieza
    Created on : 21 sep. 2020, 19:23:31
    Author     : Liz
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alta de pieza</title>
    </head>
    <body>
        <nav>
            <a href="ControladorEncargado?action=home">Inicio</a>
            <a href="ControladorEncargado?action=altapieza">Alta de pieza</a>
            <a href="ControladorEncargado?action=listadopiezas">Listado de piezas</a>
            <a href="ControladorEncargado?action=listadoventas">Listado de bicicletas vendidas</a>
            <a href="ControladorEncargado?action=registrouser">Registrar usuario</a>
            <a href="ControladorEncargado?action=usuariosreg">Usuarios registrados</a>
            <a href="ControladorLogout">Salir</a>

        </nav>

        <h1>Bicicleteria</h1>
        
        <h2>Alta Piezas </h2>
        
        <form method="post" action="ControladorStockPiezas">
          <select name="nuevaPieza">
            <c:forEach var="pieza" items="${recursos}">
                <option value="${pieza}">${pieza}</option>
            </c:forEach>
          </select>
            
          <p>
              <label for="codigo">Código</label><br>
              <input id="codigo" type="text" name="codigo" required>
          </p>
          
          <p>
              <label for="cantidad">Cantidad</label><br>
              <input id="cantidad" type="number" min="1" max="9999" name="cantidad" value="1">
          </p>         
                              
          <input type="submit" value="Alta">
        </form>
    </body>
</html>
