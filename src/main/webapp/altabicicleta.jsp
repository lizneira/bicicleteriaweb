<%-- 
    Document   : altapieza
    Created on : 21 sep. 2020, 19:23:31
    Author     : Liz
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alta de bicicleta</title>
        
        <link rel="stylesheet" href="css/estilos.css"/>
    </head>
    <body>
        <nav>
            <a href="ControladorBicicletero?action=home">Inicio</a>
            <a href="ControladorBicicletero?action=armarbicicleta">Armar bicicleta</a>
            <a href="ControladorBicicletero?action=listadobicicletas">Listado de bicicletas</a>
            <a href="ControladorLogout">Salir</a>

        </nav>

        <h1>Bicicleteria</h1>
        
        <h2>Alta de bicicleta</h2>
        
        <table class="listado">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Stock disponible</th>
                </tr>
            </thead>
            
            <tbody>
                <c:forEach var="codigoStock" items="${stock}">
                    <tr>
                        <td>${codigoStock.getCodigo()}</td>
                        <td>${codigoStock}</td>
                        <td>
                            <c:if test="${codigoStock.checkStockBicicletaCompleta() == true}">
                                <a class="armar"href="ControladorBicicletero?action=guardarBicicleta&codigo=${codigoStock.getCodigo()}">Armar</a>
                            </c:if>
                            <c:if test="${codigoStock.checkStockBicicletaCompleta() == false}">
                                <p>No existe stock suficiente</p>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
            
        </table>

    </body>
</html>
