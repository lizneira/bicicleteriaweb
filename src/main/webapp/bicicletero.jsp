<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Bicicleteria</title>
  </head>
  <body class="home-bicicletero">
    <nav>
        <a href="ControladorBicicletero?action=home">Inicio</a>
        <a href="ControladorBicicletero?action=armarbicicleta">Armar bicicleta</a>
        <a href="ControladorBicicletero?action=listadobicicletas">Listado de bicicletas</a>
    </nav>
    <div class="div-bicicletero">  
    <h1>Bicicletero</h1>
    
    <p>
        Hola ${nombre}!
    </p>

    <form method="post" action="ControladorLogout">
      <input type="submit" value="Salir" >
    </form>
    </div>
  </body>
</html>
