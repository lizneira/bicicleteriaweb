<%-- 
    Document   : venderbicicletas
    Created on : 28 sep. 2020, 14:09:18
    Author     : Liz
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Vender Bicicletas</title>
    </head>
    <body>
        <nav>
          <a href="ControladorVendedor?action=home">Inicio</a>
          <a href="ControladorVendedor?action=venderbicicletas">Vender Bicicletas</a>
          <a href="ControladorLogout">Salir</a>
       </nav>
        <h1>Vendedor</h1>
        
        <h2>Vender Bicicletas</h2>
        
        <table>
            <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Stock</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="bicicleta" items="${bicicletas}">
                    <tr>
                        <td>${bicicleta.getCodigo()}</td>
                        <td>${bicicleta.getStock()}</td>
                        <td>
                            <a class="vender" href="ControladorVendedor?action=registrarVenta&codigo=${bicicleta.getCodigo()}">Vender</a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        
    </body>
</html>


