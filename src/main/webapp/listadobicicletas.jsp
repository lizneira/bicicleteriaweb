<%-- 
    Document   : listadobicicletas
    Created on : 23 sep. 2020, 22:02:14
    Author     : Liz
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listado Bicicletas</title>
    </head>
    <body>
    <nav>  
        <a href="ControladorBicicletero?action=home">Inicio</a>
        <a href="ControladorBicicletero?action=armarbicicleta">Armar bicicleta</a>
        <a href="ControladorBicicletero?action=listadobicicletas">Listado de bicicletas</a>
        <a href="ControladorLogout">Salir</a>
    </nav>
      
    <h1>Bicicletero</h1> 
    
    <h2>Listado de Bicicletas</h2>
    
    <table>
        <thead>
            <tr>
                <th>Codigo</th>
                <th>Stock</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="bicicleta" items="${bicicletas}">
                <tr>
                    <td>${bicicleta.getCodigo()}</td>
                    <td>${bicicleta.getStock()}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
        
    </body>
</html>
