package bicicleteria;

public class Asiento extends Pieza {

    public Asiento(String codigo) {
        super(codigo);
        setTipo("Asiento");
    }

    @Override
    public String toString() {
        return "Asiento {" + getCodigo() + '}';
    }

}
