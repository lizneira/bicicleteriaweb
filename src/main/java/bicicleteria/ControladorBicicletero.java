
package bicicleteria;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Liz
 */
@WebServlet(name = "ControladorBicicletero", urlPatterns = {"/ControladorBicicletero"})
public class ControladorBicicletero extends HttpServlet {

  
    private HttpServletRequest request;
    private HttpServletResponse response;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void procesarEntrada(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");
        String vista = null;
        
        HttpSession session = request.getSession();
        
        Bicicletero bicicletero =  (Bicicletero) session.getAttribute("bicicletero");
        
        ModeloBicicletas modeloBicicletas;
        /*ModeloUsuarios modeloUsuarios;
        ArrayList<Persona> usuarios;*/    
        
        if ( bicicletero != null){              
        
            switch (action){
                case "home":

                    request.setAttribute("nombre", bicicletero.getNombre() );

                    vista = "bicicletero.jsp";
                    break;
                case "armarbicicleta":

                    ModeloStockPieza modeloStockPieza = new ModeloStockPieza();
                    
                    ArrayList<StockCodigo> stock = modeloStockPieza.getCodigosDisponibles();
                    
                    request.setAttribute("stock", stock );

                    vista = "altabicicleta.jsp";
                    break;
                    
                case "listadobicicletas":

                    modeloBicicletas = new ModeloBicicletas();
                    
                    ArrayList<StockBicicleta> bicicletas = modeloBicicletas.getBicicletas();
                    
                    request.setAttribute("bicicletas", bicicletas );

                    vista = "listadobicicletas.jsp";
                    break;
                    
                case "guardarBicicleta":
                    
                    modeloBicicletas = new ModeloBicicletas();
                    
                    String codigo = request.getParameter("codigo");
                    
                    modeloBicicletas.guardar(codigo);
                    
                    vista="ControladorBicicletero?action=listadobicicletas";
                    break;
                    

            }
        }
        else{
            vista = "index.jsp";   
        }
        
        request.getRequestDispatcher(vista).forward(request, response);
            
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        procesarEntrada(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        procesarEntrada(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String exception = e.getActionCommand();
                request.setAttribute("mensajeError", exception);
                RequestDispatcher v =  request.getRequestDispatcher("error.jsp");
                v.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
