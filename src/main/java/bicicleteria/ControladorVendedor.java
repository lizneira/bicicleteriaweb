/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bicicleteria;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Liz
 */
@WebServlet(name = "ControladorVendedor", urlPatterns = {"/ControladorVendedor"})
public class ControladorVendedor extends HttpServlet {
       private HttpServletRequest request;
       private HttpServletResponse response;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
     protected void procesarEntrada(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");
        String vista = null;
        
        HttpSession session = request.getSession();
        
        Vendedor vendedor =  (Vendedor) session.getAttribute("vendedor");
        
        ModeloBicicletas modeloBicicletas;
        
        if ( vendedor != null){              
        
            switch (action){
                case "home":

                    request.setAttribute("nombre", vendedor.getNombre() );

                    vista = "vendedor.jsp";
                    break;
                    
                case "venderbicicletas":
                    modeloBicicletas = new ModeloBicicletas();
                    
                    ArrayList<StockBicicleta> bicicletas = modeloBicicletas.getBicicletas();
                    
                    request.setAttribute("bicicletas", bicicletas );

                    vista = "venderbicicletas.jsp";
                    break;
                    
                case "registrarVenta":
                    //Eliminar una bicicleta con el codigo especificado de la tabla de bicicletas
                    modeloBicicletas = new ModeloBicicletas();
                    
                    //Codigo de la bicicleta vendida (A eliminar)
                    String codigo = request.getParameter("codigo");
                    
                    //Agregar la venta
                    modeloBicicletas.registrarVenta(codigo);
                    
                    //Recarga vista de ventas                    
                    vista = "ControladorVendedor?action=venderbicicletas";
                    break;
                
                

            }
        }
        else{
            vista = "index.jsp";   
        }
        
        request.getRequestDispatcher(vista).forward(request, response);
            
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        procesarEntrada(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        procesarEntrada(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
     private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String exception = e.getActionCommand();
                request.setAttribute("mensajeError", exception);
                RequestDispatcher v =  request.getRequestDispatcher("error.jsp");
                v.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
