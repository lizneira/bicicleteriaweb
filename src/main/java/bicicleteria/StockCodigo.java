/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bicicleteria;

/**
 *
 * @author Liz
 */
public class StockCodigo {
    private String codigo; //A
    
    private int cantManubrios;
    private int cantCuadros;
    private int cantPedales;
    private int cantRuedas;
    private int cantKitMecanico;
    private int cantAsiento;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getCantManubrios() {
        return cantManubrios;
    }

    public void setCantManubrios(int cantManubrios) {
        this.cantManubrios = cantManubrios;
    }

    public int getCantCuadros() {
        return cantCuadros;
    }

    public void setCantCuadros(int cantCuadros) {
        this.cantCuadros = cantCuadros;
    }

    public int getCantPedales() {
        return cantPedales;
    }

    public void setCantPedales(int cantPedales) {
        this.cantPedales = cantPedales;
    }

    public int getCantRuedas() {
        return cantRuedas;
    }

    public void setCantRuedas(int cantRuedas) {
        this.cantRuedas = cantRuedas;
    }

    public int getCantKitMecanico() {
        return cantKitMecanico;
    }

    public void setCantKitMecanico(int cantKitMecanico) {
        this.cantKitMecanico = cantKitMecanico;
    }

    public int getCantAsiento() {
        return cantAsiento;
    }

    public void setCantAsiento(int cantAsiento) {
        this.cantAsiento = cantAsiento;
    }
    
    public boolean checkStockBicicletaCompleta(){
        boolean hayStock = false;
        
        if ( getCantAsiento()     >= 1 &&
             getCantCuadros()     >= 1 &&
             getCantKitMecanico() >= 1 &&
             getCantPedales()     >= 2 &&
             getCantRuedas()      >= 2 &&
             getCantManubrios()   >= 1)
        {
            hayStock = true;
        }
        
        return hayStock;
    }

    @Override
    public String toString() {
        return "Manubrios (" + cantManubrios + "), Cuadros(" + cantCuadros + "), Pedales(" + cantPedales + "), Ruedas (" + cantRuedas + "), Kit Mecanico (" + cantKitMecanico + "), Asiento (" + cantAsiento + ")";
    }
    
    
    
}
