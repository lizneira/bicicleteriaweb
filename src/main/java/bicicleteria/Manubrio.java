package bicicleteria;

public class Manubrio extends Pieza {

    public Manubrio(String codigo) {
        super(codigo);
        setTipo("Manubrio");
    }

    @Override
    public String toString() {
        return "Manubrio {" + getCodigo() + '}';
    }

}
