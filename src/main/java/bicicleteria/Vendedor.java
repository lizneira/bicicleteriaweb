package bicicleteria;

public class Vendedor extends Persona {

    public Vendedor(String usuario, String password, String nombre) {
        super(usuario, password, nombre, "vendedor");
    }

    @Override
    public String getVista() {
        return "vendedor.jsp";
    }

    @Override
    public Object getRecursos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
