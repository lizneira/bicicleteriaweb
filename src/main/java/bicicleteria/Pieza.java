package bicicleteria;


public abstract class Pieza {

    private final String codigo;
    private String tipo;

    public Pieza(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getTipo() {
        return tipo;
    }

    public final void setTipo(String tipo) {
        this.tipo = tipo;
    }

}
