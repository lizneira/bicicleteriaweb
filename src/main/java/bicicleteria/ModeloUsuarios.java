package bicicleteria;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ModeloUsuarios {

    private String jdbcDriver;
    private String dbName;
    private String urlRoot;
    private ActionListener listener;

    public ModeloUsuarios() {
        jdbcDriver = "com.mysql.cj.jdbc.Driver";
        urlRoot = "jdbc:mysql://127.0.0.1/";
        dbName = "bicicleteria";
        listener = null;
        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            reportException(e.getMessage());
        }
    }
    
    public ArrayList<Persona> getUsuarios() {
        
        ArrayList<Persona> usuarios = new ArrayList<>();
        
        
        try {
            Connection con = DriverManager.getConnection(urlRoot + dbName + "?user=root&password=");
            Statement stmt = con.createStatement();
            
            stmt.execute("SELECT nombre, rol, password, usuario FROM personas");
            
            ResultSet rs = stmt.getResultSet();
            
            Persona usuario = null;
            
            while (rs.next()) {
                /** @TODO: Agregar Factory **/
                switch (rs.getString("rol")) {
                    case "bicicletero":
                        usuario = new Bicicletero(rs.getString("usuario"), rs.getString("password"), rs.getString("nombre"));
                        break;
                    case "encargado":
                        usuario = new Encargado(rs.getString("usuario"), rs.getString("password"), rs.getString("nombre"));
                        break;
                    case "vendedor":
                        usuario = new Vendedor(rs.getString("usuario"), rs.getString("password"), rs.getString("nombre"));
                        break;
                }
                
                usuarios.add(usuario);
            }
            stmt.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            
            reportException(e.getMessage());
        }
        
        return usuarios;
    }

    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }

    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }
}
