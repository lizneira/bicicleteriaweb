package bicicleteria;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Liz
 */
@WebServlet(name = "ControladorEncargado", urlPatterns = {"/ControladorEncargado"})
public class ControladorEncargado extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");
        String vista = null;
        
        HttpSession session = request.getSession();
        
        Encargado encargado =  (Encargado) session.getAttribute("encargado");
        
        ModeloUsuarios modeloUsuarios;
        ArrayList<Persona> usuarios;      
        
        if ( encargado != null){              
        
            switch (action){
                case "home":

                    request.setAttribute("nombre", encargado.getNombre() );

                    vista = "encargado.jsp";
                    break;

                case "altapieza":

                    request.setAttribute("recursos", encargado.getRecursos() );                

                    vista = "altapieza.jsp";
                    break;

                case "listadopiezas":
                    ModeloStockPieza modeloStockPieza = new ModeloStockPieza();

                    ArrayList<Pieza> piezas = modeloStockPieza.getPiezas();

                    request.setAttribute("piezas", piezas);
                    vista = "listadopiezas.jsp";
                    break;

                case "listadoventas":
                    ModeloBicicletas modeloBicicletas = new ModeloBicicletas();
                      
                    ArrayList<Venta> ventas = modeloBicicletas.getListadoVentas();
                    
                    request.setAttribute("ventas", ventas);
                    
                    vista = "listadoventas.jsp";
                    break;

                case "usuariosreg":
                    modeloUsuarios = new ModeloUsuarios();

                    usuarios = modeloUsuarios.getUsuarios();

                    request.setAttribute("usuarios", usuarios);

                    vista = "listadousuarios.jsp";
                    break;

                case "registrouser":
                    vista = "registro.jsp";

                    break;

                case "guardaruser":
                    String nombre = request.getParameter("nombre");
                    String usuario = request.getParameter("usuario");
                    String password = request.getParameter("password");
                    String rol = request.getParameter("rol");

                    Persona persona = null;

                    switch(rol){
                        case "vendedor":
                            persona = new Vendedor(usuario, password, nombre);
                            break;

                        case "bicicletero":
                            persona = new Bicicletero(usuario, password, nombre);       
                            break;
                    }

                    ModeloLogin modeloLogin = new ModeloLogin();

                    modeloLogin.registrar(persona);



                        request.setAttribute("nombre", encargado.getNombre());
                        request.setAttribute("recursos", encargado.getRecursos());

                        modeloUsuarios = new ModeloUsuarios();

                        usuarios = modeloUsuarios.getUsuarios();

                        request.setAttribute("usuarios", usuarios);

                        vista = encargado.getVista();

                    break;

            }
        }
        else{
            vista = "index.jsp";   
        }
        
        request.getRequestDispatcher(vista).forward(request, response);
                
               
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
