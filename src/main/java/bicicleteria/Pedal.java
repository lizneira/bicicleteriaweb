package bicicleteria;

public class Pedal extends Pieza {

    public Pedal(String codigo) {
        super(codigo);
        setTipo("Pedal");
    }

    @Override
    public String toString() {
        return "Pedal {" + getCodigo() + '}';
    }

}
