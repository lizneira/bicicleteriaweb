/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bicicleteria;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Liz
 */
public class ModeloBicicletas {
    
    private String jdbcDriver;
    private String dbName;
    private String urlRoot;
    private ActionListener listener;

    public ModeloBicicletas() {
        jdbcDriver = "com.mysql.cj.jdbc.Driver";
        urlRoot = "jdbc:mysql://127.0.0.1/";
        dbName = "bicicleteria";
        listener = null;
        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            reportException(e.getMessage());
        }
    }

    public ArrayList<StockBicicleta> getBicicletas() {
        
        ArrayList<StockBicicleta> bicicletas = new ArrayList<>();

        try {
            Connection con = DriverManager.getConnection(urlRoot + dbName + "?user=root&password=");
            Statement stmt = con.createStatement();
            
            stmt.execute("SELECT codigo, COUNT(*) AS stock FROM bicicletas GROUP BY codigo");
            
            ResultSet rs = stmt.getResultSet();

            StockBicicleta stockBicicleta;
            
            while (rs.next()) {
  
                stockBicicleta = new StockBicicleta();
               
                stockBicicleta.setCodigo(rs.getString("codigo"));
                stockBicicleta.setStock(rs.getInt("stock"));
                
                bicicletas.add(stockBicicleta); 
            }
            
            stmt.close();
            rs.close();
            con.close();
          
        } catch (SQLException e) {
            reportException(e.getMessage());
        }
        
        return bicicletas;
    }
       
    public void guardar(String codigo){
    
        //Eliminar una pieza de cada tipo (excepto pedales y ruedas que son dos)
        
        eliminarPieza(codigo, "Asiento");
        eliminarPieza(codigo, "Manubrio");
        eliminarPieza(codigo, "Pedal");
        eliminarPieza(codigo, "Rueda");
        eliminarPieza(codigo, "Cuadro");
        eliminarPieza(codigo, "KitMecanico");
        
        //Agregar la bicicleta en la tabla de bicicletas (Con el codigo)
        try{
            Connection con = DriverManager.getConnection(urlRoot + dbName + "?user=root&password=");
            Statement stmt = con.createStatement();

            String sql = "INSERT INTO bicicletas(codigo) VALUES ('" + codigo + "')";
            
            stmt.executeUpdate(sql);
            
            stmt.close();
            con.close();
            
        }
        catch(SQLException ex){
            
        }
    }
    
    private void eliminarPieza(String codigo, String tipoPieza){
        try{
            Connection con = DriverManager.getConnection(urlRoot + dbName + "?user=root&password=");
            Statement stmt = con.createStatement();
            Statement stmtDelete = con.createStatement();

            String sql = "SELECT id, codigo, pieza FROM piezas WHERE codigo = '" + codigo + "' AND pieza = '" + tipoPieza + "'";

            stmt.execute(sql);

            ResultSet rs = stmt.getResultSet();

            int cantidad = 1;
            
            if ( tipoPieza.equals("Pedal") || tipoPieza.equals("Rueda")){
                cantidad = 2;
            }
            
            for(int i=1; i<=cantidad; i++){
                rs.next();

                int id = rs.getInt("id");

                //Borra el registro con este ID
                stmtDelete.execute("DELETE FROM piezas WHERE id=" + id);  
            }
            
            stmt.close();
            stmtDelete.close();
            con.close();
            

      }  
      catch(SQLException ex){

      }
    }
    
    public void registrarVenta(String codigo){
        try {
            Connection con = DriverManager.getConnection(urlRoot + dbName + "?user=root&password=");
            Statement stmt = con.createStatement();

            String sql = "SELECT id, codigo FROM bicicletas WHERE codigo = '" + codigo + "'";
            
            stmt.execute(sql);

            ResultSet rs = stmt.getResultSet();
            
            rs.next();
            
            int id = rs.getInt("id");
            
            stmt.execute("DELETE FROM bicicletas WHERE id=" + id);
            
            stmt.execute("INSERT INTO ventas(codigo) VALUES('" + codigo + "')");
            
            rs.close();
            stmt.close();
            con.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(ModeloBicicletas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ArrayList<Venta> getListadoVentas(){
        
        ArrayList<Venta> listadoVentas = new ArrayList<>();
        
      try {
            Connection con = DriverManager.getConnection(urlRoot + dbName + "?user=root&password=");
            Statement stmt = con.createStatement();

            String sql = "SELECT id, codigo, fecha FROM ventas";
            
            stmt.executeQuery(sql);

            ResultSet rs = stmt.getResultSet();
            
            while( rs.next() ){
                
                Venta venta = new Venta();
                
                venta.setId( rs.getInt("id") );
                venta.setCodigo( rs.getString("codigo"));
                venta.setFecha( rs.getDate("fecha"));
              
                listadoVentas.add(venta);
                
            }
            
            rs.close();
            stmt.close();
            con.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(ModeloBicicletas.class.getName()).log(Level.SEVERE, null, ex);
        }
      
      return listadoVentas;
    }
    
    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }

    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }
}
