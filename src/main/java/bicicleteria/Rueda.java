package bicicleteria;

public class Rueda extends Pieza {

    public Rueda(String codigo) {
        super(codigo);
        setTipo("Rueda");
    }

    @Override
    public String toString() {
        return "Rueda {" + getCodigo() + '}';
    }

}
