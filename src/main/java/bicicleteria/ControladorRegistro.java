
package bicicleteria;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Liz
 */
@WebServlet(name = "ControladorRegistro", urlPatterns = {"/ControladorRegistro"})
public class ControladorRegistro extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /*String nombre = request.getParameter("nombre");
        String usuario = request.getParameter("usuario");
        String password = request.getParameter("password");
        String rol = request.getParameter("rol");
        
        Persona persona = null;
        
        switch(rol){
            case "vendedor":
                persona = new Vendedor(usuario, password, nombre);
                break;
            
            case "bicicletero":
                persona = new Bicicletero(usuario, password, nombre);       
                break;
        }
        
        ModeloLogin modeloLogin = new ModeloLogin();
        
        modeloLogin.registrar(persona);
        
 
        HttpSession session = request.getSession();
        
        Encargado encargado = (Encargado) session.getAttribute("encargado");
        
        if ( encargado != null){
            
            request.setAttribute("nombre", encargado.getNombre());
            request.setAttribute("recursos", encargado.getRecursos());
            
            ModeloUsuarios modeloUsuarios = new ModeloUsuarios();
            
            ArrayList<Persona> usuarios = modeloUsuarios.getUsuarios();
            
            request.setAttribute("usuarios", usuarios);
            
            request.getRequestDispatcher(encargado.getVista()).forward(request, response);
        }  
        else{
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }*/
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
