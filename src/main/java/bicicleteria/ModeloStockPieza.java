package bicicleteria;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ModeloStockPieza {
    
    private String jdbcDriver;
    private String dbName;
    private String urlRoot;
    private ActionListener listener;
    public ModeloStockPieza() {
        jdbcDriver = "com.mysql.cj.jdbc.Driver";
        urlRoot = "jdbc:mysql://127.0.0.1/";
        dbName = "bicicleteria";
        listener = null;
        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            reportException(e.getMessage());
        }
    }

    public void alta(String pieza, String codigo, int cantidad) {
        try {
            Connection con = DriverManager.getConnection(urlRoot + dbName + "?user=root&password=");
            Statement stmt = con.createStatement();
            
            for(int i=0; i < cantidad; i++){ 
                stmt.executeUpdate("INSERT INTO piezas(pieza,codigo) VALUES('"+pieza+"','"+codigo+"');");
            }
            
            stmt.close();
        } catch(SQLException ex) {
            reportException(ex.getMessage());
        }
    }
    
    public ArrayList<Pieza> getPiezas() {
        
        ArrayList<Pieza> piezas = new ArrayList<>();

        try {
            Connection con = DriverManager.getConnection(urlRoot + dbName + "?user=root&password=");
            Statement stmt = con.createStatement();
            
            stmt.execute("SELECT codigo, pieza FROM piezas");
            
            ResultSet rs = stmt.getResultSet();
            
            Pieza pieza = null;
            
            while (rs.next()) {
                switch(rs.getString("pieza")){
                    case "Asiento":
                        pieza = new Asiento(rs.getString("codigo"));
                        break;
                    case "Cuadro":
                        pieza = new Cuadro(rs.getString("codigo"));
                        break;
                    case "KitMecanico":
                        pieza = new KitMecanico (rs.getString("codigo"));
                        break;
                    case "Manubrio":
                        pieza = new Manubrio(rs.getString("codigo"));
                        break;
                    case "Rueda":
                        pieza = new Rueda(rs.getString("codigo"));
                        break;
                    case "Pedal":
                        pieza = new Pedal(rs.getString("codigo"));
                        break;
                       
                }
                
                piezas.add(pieza);
            }
            stmt.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            
            reportException(e.getMessage());
        }
        
        return piezas;
    }
    
    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }

    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }
    
    public ArrayList<StockCodigo> getCodigosDisponibles(){
        
        ArrayList<StockCodigo> stock = new ArrayList<>();

        String codigo;
        
        try {
            Connection con = DriverManager.getConnection(urlRoot + dbName + "?user=root&password=");
            Statement stmt = con.createStatement();
            Statement stmtStockCodigo = con.createStatement();
            
            stmt.execute("SELECT DISTINCT(codigo) FROM piezas");
            
            ResultSet rs = stmt.getResultSet();
            
            ResultSet rsStockCodigo;
            
            while (rs.next()) {
                
                StockCodigo stockCodigo = new StockCodigo();
                
                codigo = rs.getString("codigo");
                
                stockCodigo.setCodigo(codigo);
                
                stmtStockCodigo.execute("SELECT pieza, COUNT(pieza) AS cantidad FROM piezas WHERE codigo='" + codigo + "' GROUP BY pieza");
                
                rsStockCodigo = stmtStockCodigo.getResultSet();
                
                while (rsStockCodigo.next()){
                    
                    switch(rsStockCodigo.getString("pieza")){
                        case "Cuadro":
                            stockCodigo.setCantCuadros(rsStockCodigo.getInt("cantidad"));
                            break;
                            
                         case "Asiento":
                            stockCodigo.setCantAsiento(rsStockCodigo.getInt("cantidad"));
                            break;   
                        
                        case "Rueda":
                            stockCodigo.setCantRuedas(rsStockCodigo.getInt("cantidad"));
                            break;  
                            
                        case "Pedal":
                            stockCodigo.setCantPedales(rsStockCodigo.getInt("cantidad"));
                            break;  
                         
                        case "Manubrio":
                            stockCodigo.setCantManubrios(rsStockCodigo.getInt("cantidad"));
                            break;  
                        
                        case "KitMecanico":
                            stockCodigo.setCantKitMecanico(rsStockCodigo.getInt("cantidad"));
                            break;  
                    }
                }
                
                stock.add(stockCodigo);
            }
        }
        catch(SQLException e){
            reportException(e.getMessage());
        }
        
        return stock;
    }
    
    
}
