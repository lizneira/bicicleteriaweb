package bicicleteria;


public class Bicicleta{

    private Asiento a;
    private Cuadro c;
    private KitMecanico km;
    private Manubrio m;
    private Pedal pi;
    private Pedal pd;
    private Rueda rd;
    private Rueda rt;

    /**
     * @return the a
     */
    public Asiento getA() {
        return a;
    }

    /**
     * @param a the a to set
     */
    public void setA(Asiento a) {
        this.a = a;
    }

    /**
     * @return the c
     */
    public Cuadro getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(Cuadro c) {
        this.c = c;
    }

    /**
     * @return the km
     */
    public KitMecanico getKm() {
        return km;
    }

    /**
     * @param km the km to set
     */
    public void setKm(KitMecanico km) {
        this.km = km;
    }

    /**
     * @return the m
     */
    public Manubrio getM() {
        return m;
    }

    /**
     * @param m the m to set
     */
    public void setM(Manubrio m) {
        this.m = m;
    }

    /**
     * @return the pi
     */
    public Pedal getPi() {
        return pi;
    }

    /**
     * @param pi the pi to set
     */
    public void setPi(Pedal pi) {
        this.pi = pi;
    }

    /**
     * @return the pd
     */
    public Pedal getPd() {
        return pd;
    }

    /**
     * @param pd the pd to set
     */
    public void setPd(Pedal pd) {
        this.pd = pd;
    }

    /**
     * @return the rd
     */
    public Rueda getRd() {
        return rd;
    }

    /**
     * @param rd the rd to set
     */
    public void setRd(Rueda rd) {
        this.rd = rd;
    }

    /**
     * @return the rt
     */
    public Rueda getRt() {
        return rt;
    }

    /**
     * @param rt the rt to set
     */
    public void setRt(Rueda rt) {
        this.rt = rt;
    }

    @Override
    public String toString() {
        return "Bicicleta {" + a.getCodigo() + '}';
    }

}
