package bicicleteria;

public class Cuadro extends Pieza {

    public Cuadro(String codigo) {
        super(codigo);
        setTipo("Cuadro");
    }

    @Override
    public String toString() {
        return "Cuadro {" + getCodigo() + '}';
    }

}
