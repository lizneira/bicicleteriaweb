package bicicleteria;

public class KitMecanico extends Pieza {

    public KitMecanico(String codigo) {
        super(codigo);
        setTipo("Kit Mecánico");
    }

    @Override
    public String toString() {
        return "Kit Mecánico {" + getCodigo() + '}';
    }

}
